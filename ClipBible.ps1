# Load settings
$Config = Get-Content "$PSScriptRoot\ClipBible.json" | ConvertFrom-Json

# .NET Framework classes
Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName System.Windows.Forms

# Create UI with XAML
[xml]$XAML = Get-Content "$PSScriptRoot\ClipBible.xaml"
$XAML.Window.RemoveAttribute('x:Class')
$XAML.Window.RemoveAttribute('mc:Ignorable')
$MainForm = [Windows.Markup.XamlReader]::Load((New-Object System.Xml.XmlNodeReader $XAML))

# Init grid datasource
$Script:ReferencesSource = New-Object System.Collections.ObjectModel.ObservableCollection[System.Object]
$MainForm.FindName("ReferencesGrid").ItemsSource = $Script:ReferencesSource

# Load language
function Load-Language($Language)
{
    $Script:SelectedLanguage = $Script:Config.Languages.($Language)

    # Load Bible
    $BibleCsv = Import-Csv "$PSScriptRoot\$(($Script:SelectedLanguage).BibleFilename)" -Delimiter ";" 
    $Script:Bible = @{}
    foreach($Verse in $BibleCsv)
    {
        if(-not $Script:Bible.ContainsKey($Verse.bookNumber)) { $Script:Bible[$Verse.bookNumber] = @{} }
        if(-not $Script:Bible[$Verse.bookNumber].ContainsKey($Verse.chapterNumber)) { $Script:Bible[$Verse.bookNumber][$Verse.chapterNumber] = @{} }
        $Script:Bible[$Verse.bookNumber][$Verse.chapterNumber][$Verse.verseNumber] = $Verse.content
    }

    # Compose detection regex
    $DetectionRegexString = "\s*("+(($Script:SelectedLanguage.Books.PSObject.Properties.Name) -join "|")+")\.?\s*(?:(\d+):|)(\d+(?:(?:[,-][\s]{0,1}\d+)+|))(?:;\s*(\d+):(\d+(?:(?:[,-][\s]{0,1}\d+)+|)))?"
    $Script:DetectionRegex = [regex]::new($DetectionRegexString, [System.Text.RegularExpressions.RegexOptions]::IgnoreCase)
}

# Setup language combobox
$MainForm.FindName("LanguageComboBox").ItemsSource = ($Config.Languages | Get-Member -MemberType NoteProperty | Select-Object -ExpandProperty Name)
$MainForm.FindName("LanguageComboBox").SelectedValue = $Config.Default.Language
$MainForm.FindName("LanguageComboBox").add_SelectionChanged({
    $SelValue = ($MainForm.FindName("LanguageComboBox").SelectedValue)
    if($null -eq $SelValue -or $SelValue -eq "")
    {
        Write-Host "Is null!"
        return
    }
    Load-Language -Language $SelValue
})
Load-Language -Language $Config.Default.Language

# Setup checkBox default value
$MainForm.FindName("CopyFoundReferenceCheckBox").IsChecked = $Config.Default.CopyFoundReference

# Grid click event
$MainForm.FindName("ReferencesGrid").add_MouseLeftButtonUp({
	$Row = $MainForm.FindName("ReferencesGrid").SelectedItems[0]
	$MainForm.FindName("ReferenceTextBox").Text = $Row.Reference
}) 

# Grid double click event
$MainForm.FindName("ReferencesGrid").add_MouseDoubleClick({
	$Row = $MainForm.FindName("ReferencesGrid").SelectedItems[0]
	Start-Process $Row.Url
}) 

# Range regex
$RangeRegex = [regex]::new("(\d+)-(\d+)")

# A reference could be: Genesi 1:2,3,6-10;2:5,6
function Get-References($Ref)
{
    # Book is one
    $BookNumber = [int]($Script:SelectedLanguage.Books.($Ref.Groups[1].Value.ToLower()))
    $IsOneChapterBook = @(57,63,64,65) -contains $BookNumber

    # References could be multiple
    $References = @()
    $ReferencesCount = (($Ref.Groups | Where-Object {$_.Success}).Count - 2) / 2
    for($RefIndex = 0; $RefIndex -lt $ReferencesCount; $RefIndex++)
    {
        $Verses = $Ref.Groups[3 + $RefIndex * 2].Value
        # If it's a single chapter book, ignore the chapter in the reference
        if($IsOneChapterBook)
        {
            $Chapter = 1
            $Reference = "$(Get-ExtendedBookName $BookNumber) $Verses"    
        }else{
            $Chapter = [int]($Ref.Groups[2 + $RefIndex * 2].Value)
            $Reference = "$(Get-ExtendedBookName $BookNumber) $Chapter`:$Verses"
        }
        $VersesContent = @()
    
        foreach($SplittedVerse in $Verses.Split(","))
        {
            $RangeMatches = $RangeRegex.Matches($SplittedVerse)
            if($RangeMatches.Count -gt  0)
            {
                # If it's a range
                $FromVerse = [int]($RangeMatches[0].Groups[1].Value)
                $ToVerse = [int]($RangeMatches[0].Groups[2].Value)
                for($Verse = $FromVerse; $Verse -le $ToVerse; $Verse++)
                {
                    $Content = $Script:Bible."$BookNumber"."$Chapter"."$Verse"
                    if($Content -eq $null){return}
                    $VersesContent += "$Verse $Content"
                }
            }else{
                # If it's a single verse
                $Verse = [int]$SplittedVerse
                $Content = $Script:Bible."$BookNumber"."$Chapter"."$Verse"
                if($Content -eq $null){return}
                $VersesContent += "$Verse $Content"
            }
        }

        $References += @{
            Reference = $Reference
            Text = ($VersesContent -join " ").Replace("`r`n"," ").Replace("`n"," ")
            Url = "https://wol.jw.org/it/wol/b/r6/lp-i/nwtsty/$BookNumber/$Chapter"
        }
    }
    return $References
}

function Get-ExtendedBookName([int]$BookNumber)
{
    $Book = $Script:SelectedLanguage.Books.PSObject.Properties | Where-Object {$_.Value -eq $BookNumber } | Sort-Object -Property {$_.Name.length} -Descending
    return $Book[0].Name
}

function Search-Ref($Text)
{
    # Search for a scripture in the regex
    $RefMatches = $Script:DetectionRegex.Matches($Text)

    # Process all matches
    foreach($RefMatch in $RefMatches)
    {
        # Add found references
        $References = Get-References $RefMatch
        $References | % { $Script:ReferencesSource.Insert(0,(New-Object PSObject -Property $_)) }
    }

    # Scroll down and copy text
    if($RefMatches.Count -gt 0) { 
        # Scroll to the first reference of the group
        $Grid = $MainForm.FindName("ReferencesGrid")
        $Items = $Grid.Items
        $Item = $Items[0] #$Items[$Script:ReferencesSource.Count]
        if($Item -ne $null){ $Grid.ScrollIntoView($Item) }
    }

    #Write-Host "Found ref $($RefMatches.Count)"

    return $RefMatches.Count -gt 0
}

# Search for references in the clipboard
$global:LastClip = Get-Clipboard
$Script:timer = New-Object System.Windows.Forms.Timer -Property @{Interval = $Script:Config.ClipboardSearchInterval} 
$Script:timer.start()
$Script:timer.add_Tick({ 
	# Get clipboard content
    $Clip = Get-Clipboard -Raw
    $Grid = $MainForm.FindName("ReferencesGrid")
    if($Clip -ne $global:LastClip)
    {
        # Store last processed clipboard
        $global:LastClip = $Clip
        
        # Search for a scripture in the regex
        if(Search-Ref($Clip))
        {
            $Items = $Grid.Items
            $Item = $Items[0] #$Items[$Grid.Items.Count-1]
            if($MainForm.FindName("CopyFoundReferenceCheckBox").IsChecked)
            {
                $global:LastClip = "($($Item.Reference)) $($Item.Text)"
                $global:LastClip | Set-Clipboard
            }
        }
    }
})

# Search for reference in the textbox
$SearchTimer = New-Object System.Windows.Forms.Timer
$SearchTimer.Interval = $Config.TextBoxSearchDelay
$SearchTimer.add_Tick({
    $SearchTimer.Stop()
    $RefMatchesCount = Search-Ref($MainForm.FindName("ReferenceTextBox").Text)
    <#if($RefMatchesCount -gt 0)
    {
        $MainForm.FindName("ReferenceTextBox").Background = "#aaffaa"
    }else{
        $MainForm.FindName("ReferenceTextBox").Background = "#ffaaaa"
    }#>
})
$MainForm.FindName("ReferenceTextBox").Add_KeyDown({
    if ($SearchTimer.Enabled) {
        $SearchTimer.Stop()
    }
    $SearchTimer.Start()
})

# Copy-paste handling of the grid (copy the selected cell value instead of the entire row)
$RowClipboardHandler = {
	param($sender, $event)
	$currentCell = $event.ClipboardRowContent[ $MainForm.FindName("ReferencesGrid").CurrentCell.Column.DisplayIndex];
	$event.ClipboardRowContent.Clear();
	$event.ClipboardRowContent.Add($currentCell);
}
$MainForm.FindName("ReferencesGrid").Add_CopyingRowClipboardContent($RowClipboardHandler)

# Show window
$MainForm.ShowDialog()
