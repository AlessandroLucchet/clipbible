# ClipBible - The Bible in your clipboard

![Logo](ClipBible.png "ClipBible")


Need to quickly find Bible references while taking notes? 
_This is the script for you!_

It will search for Bible references in your clipboard. 
If any references are found, they will be displayed in the grid (and maybe copied back to your clipboard). 

![Screenshot](Screenshot.png "ClipBible")